import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { Login, NuevaCuenta, Proyectos } from './components/index';

import './App.css';

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={Login} />
        <Route exact path="/nueva-cuenta" component={NuevaCuenta} />
        <Route exact path="/proyectos" component={Proyectos} />
      </Switch>
    </Router>
  );
}

export default App;

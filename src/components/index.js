import Login from './auth/Login';
import NuevaCuenta from './auth/NuevaCuenta';
import Proyectos from './Proyectos/Proyectos';

export {
    Login,
    NuevaCuenta,
    Proyectos
}